﻿using ExpressiveAnnotations.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class OrderDetailsView
    {
        // todo: replace RequiredIf with custom implementation or library for .Net Core
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public int? BuildingNumber { get; set; }

        public int? ApartmentNumber { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string PostalCode { get; set; }

        public bool DifferentDeliveryAddress { get; set; }

        [RequiredIf("DifferentDeliveryAddress == true")]
        public string DeliveryStreet { get; set; }

        [RequiredIf("DifferentDeliveryAddress == true")]
        public int? DeliveryBuildingNumber { get; set; }

        public int? DeliveryApartmentNumber { get; set; }

        [RequiredIf("DifferentDeliveryAddress == true")]
        public string DeliveryCity { get; set; }

        [RequiredIf("DifferentDeliveryAddress == true")]
        public string DeliveryPostalCode { get; set; }

        [Required]
        public byte PaymentProviderId { get; set; }

        [Required]
        public ShoppingCartEncodedData ShoppingCartEncoded { get; set; }
        public List<ShoppingCartProductViewModel> ShoppingCartProducts { get; set; }
        public List<PaymentProvider> PaymentProviders { get; set; }

        public UserAddress GetInvoiceUserAddress()
        {
            var invoiceAddress = new UserAddress
            {
                Street = Street,
                BuildingNumber = BuildingNumber.GetValueOrDefault(),
                ApartmentNumber = ApartmentNumber,
                PostalCode = PostalCode,
                City = City
            };

            return invoiceAddress;
        }

        /// <summary>
        /// Returns delivery address
        /// </summary>
        /// <returns>delivery address or null if delivery address is invoice address</returns>
        public UserAddress GetDeliveryAddress()
        {
            UserAddress deliveryAddress = null;

            if(DifferentDeliveryAddress)
            {
                deliveryAddress = new UserAddress
                {
                    Street = DeliveryStreet,
                    BuildingNumber = DeliveryBuildingNumber.GetValueOrDefault(),
                    ApartmentNumber = DeliveryApartmentNumber,
                    PostalCode = DeliveryPostalCode,
                    City = DeliveryCity
                };
            }//if

            return deliveryAddress;
        }

        public InvoiceUserData GetInvoiceUserData()
        {
            var invoice = new InvoiceUserData
            {
                Name = Name,
                Surname = Surname,
                Address = GetInvoiceUserAddress()
            };

            return invoice;
        }
    }
}
