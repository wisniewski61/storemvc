﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class UserViewModel
    {
        public string Username { get; set; }

        [Display(Name = "Administrator")]
        public Boolean IsAdmin { get; set; }
    }
}
