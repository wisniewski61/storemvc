﻿using Microsoft.EntityFrameworkCore;
using Store.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class ShoppingCartEncodedData
    {
        [Required]
        public string productsString { get; set; }

        protected const char prodIdsWithQuantitySeparator = ';';
        protected const char prodIdAndQuantitySeparator = ':';

        public async Task<List<ShoppingCartProductViewModel>> GetShoppingCartItemsAsync(ApplicationDbContext dbContext)
        {
            var productsInCart = new List<ShoppingCartProductViewModel>();

            if (productsString != null && productsString.Length > 0)
            {
                var prodIdsWithQuantity = productsString.Split(prodIdsWithQuantitySeparator);
                foreach (var p in prodIdsWithQuantity)
                {
                    if (p.Length == 0)
                        continue;

                    var idQuantity = p.Split(prodIdAndQuantitySeparator);

                    if (idQuantity.Length < 2)
                        continue;

                    var productId = int.Parse(idQuantity[0]);
                    var quantity = int.Parse(idQuantity[1]);

                    var product = await dbContext.Product.SingleOrDefaultAsync(prod => prod.Id == productId);
                    if (product != null)
                    {
                        var productView = new ShoppingCartProductViewModel
                        {
                            Product = product,
                            Quantity = quantity
                        };

                        productsInCart.Add(productView);
                    }//if
                }//for
            }//if

            return productsInCart;
        }
    }
}
