﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class ProductsInCartListSessionModel
    {
        public List<ProductInCartSessionModel> Products { get; set; }

        public ProductsInCartListSessionModel()
        {
            Products = new List<ProductInCartSessionModel>();
        }
    }
}
