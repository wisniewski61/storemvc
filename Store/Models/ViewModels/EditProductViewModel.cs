﻿using Microsoft.AspNetCore.Http;
using Store.Data;
using Store.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class EditProductViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }

        [Required]
        public int? Rating { get; set; }

        [Required]
        public decimal? Price { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        // todo: custom validation, name not as string?
        [Display(Name = "Image")]
        [ImageFileEditProductViewRequired]
        public IFormFile ImageFile { get; set; }
        public byte[] Image { get; set; }

        [Required]
        [Display(Name = "Category")]
        public byte CategoryId { get; set; }

        //public string CategoryName { get; set; }

        public IEnumerable<ProductCategory> ProductCategories { get; set; }

        public EditProductViewModel()
        {
        }

        public EditProductViewModel(Product product, ApplicationDbContext context)
        {
            Id = product.Id;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            Rating = product.Rating;
            Image = product.Image;
            CategoryId = product.CategoryId;
            ProductCategories = context.ProductCategory.ToList();
        }

        /// <summary>
        /// Converts ImageFile property to bytes array
        /// </summary>
        /// <returns>Image bytes of ImageFile</returns>
        public byte[] GetImageBytes()
        {
            if (ImageFile != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    // todo: make this method called async?
                    ImageFile.CopyTo(memoryStream);
                    // TODO: image validation to prevent various attacks, see below
                    // https://docs.microsoft.com/pl-pl/aspnet/core/mvc/models/file-uploads?view=aspnetcore-2.2
                    // https://stackoverflow.com/questions/4535627/where-is-the-best-place-to-save-images-from-users-upload
                    return memoryStream.ToArray();
                }// using
            }// if

            return null;
        }

        /// <summary>
        /// Converts current object to domain model
        /// </summary>
        /// <returns>Domain model (Product)</returns>
        public Product ToDomainModel()
        {
            var imageBytes = GetImageBytes();

            if(imageBytes == null)
            {
                // use old (current) image
                imageBytes = Image;
            }

            var product = new Product
            {
                Id = Id,
                Name = Name,
                Description = Description,
                Price = Price.GetValueOrDefault(),
                Rating = Rating.GetValueOrDefault(),
                Image = imageBytes,
                CategoryId = CategoryId
            };

            return product;
        }
    }
}
