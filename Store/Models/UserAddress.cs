﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    [PersonalData]
    public class UserAddress
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Street { get; set; }
        public int BuildingNumber { get; set; }
        public int? ApartmentNumber { get; set; }

        [Required]
        [MaxLength(32)]
        public string City { get; set; }

        [Required]
        [RegularExpression("^\\d{2}-\\d{3}")]
        public string PostalCode { get; set; }
    }
}
