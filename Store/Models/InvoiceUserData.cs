﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Store.Models
{
    // todo: download personal data does not contain this table (/Identity/Account/Manage/PersonalData)
    [PersonalData]
    public class InvoiceUserData
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required]
        [MaxLength(32)]
        public string Surname { get; set; }

        [Required]
        public UserAddress Address { get; set; }
    }
}
