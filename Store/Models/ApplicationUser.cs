﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    // customization for default user identity
    public class ApplicationUser : IdentityUser
    {
        [PersonalData]
        public InvoiceUserData InvoiceUserData { get; set; }
    }
}
