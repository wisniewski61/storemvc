﻿using System;
using System.Collections.Generic;

namespace Store.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDateTime { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public InvoiceUserData InvoiceUserData { get; set; }

        // this field will be filled only when delivery adderss is different than invoice address
        public UserAddress DeliveryAddress { get; set; }

        // filed only for registered users
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
