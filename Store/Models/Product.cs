﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }
        public int Rating { get; set; }
        public decimal Price { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        // todo: check performance, maybe different table is an good idea?
        public byte[] Image { get; set; }

        public ProductCategory Category { get; set; }
        public byte CategoryId { get; set; }
    }
}
