﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class ProductCategory
    {
        // By defualt byte is not marked as IDENTITY(1, 1), so it becomes PK without autoincrementation. Fix below.
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [StringLength(24)]
        [Display(Name = "Category")]
        public string Name { get; set; }
    }
}
