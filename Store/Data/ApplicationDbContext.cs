﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Store.Models;
using System.Linq;

namespace Store.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }
        public DbSet<PaymentProvider> PaymentProviders { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<PaymentProvider>().HasData(
                new PaymentProvider { Id = 1, ProviderName = "PayPal" },
                new PaymentProvider { Id = 2, ProviderName = "Google Pay" }
            );

            builder.Entity<ProductCategory>().HasData(
                new ProductCategory { Id = 1, Name = "Computers" },
                new ProductCategory { Id = 2, Name = "Mobile phones" },
                new ProductCategory { Id = 3, Name = "Cars" },
                new ProductCategory { Id = 4, Name = "Watches" },
                new ProductCategory { Id = 5, Name = "Coffe machines" }
            );

            builder.Entity<Product>().HasData(
                new Product
                {
                    Id = 1,
                    Name = "Fictional Computer Model 999",
                    //Category = ProductCategory.SingleOrDefault(c => c.Name == "Computers"),
                    CategoryId = 1,
                    Description = "This is fictional computer. There should be some sample specification.",
                    Price = 3000
                },
                new Product
                {
                    Id = 2,
                    Name = "Unreal PC 1333",
                    CategoryId = 1,
                    Description = "This is fictional PC computer. It is working great in my mind.",
                    Price = 1000
                },
                new Product
                {
                    Id = 3,
                    Name = "Ultrabook",
                    CategoryId = 1,
                    Description = "Finctional ultrabook. Just imagine how good it might look.",
                    Price = 4000
                },
                new Product
                {
                    Id = 4,
                    Name = "Mega Phone 3819",
                    CategoryId = 2,
                    Description = "This mega phone could be used anywhere you want!",
                    Price = 500
                },
                new Product
                {
                    Id = 5,
                    Name = "Super smartphone",
                    CategoryId = 2,
                    Description = "Super smartphone. Triple boot: Android, iOS and Windows Mobile in one device!!! It is not real at all...",
                    Price = 2999.99M
                },
                new Product
                {
                    Id = 6,
                    Name = "Glorious car",
                    CategoryId = 3,
                    Description = "This car could win all really series in the world!",
                    Price = 300000.49M
                },
                new Product
                {
                    Id = 7,
                    Name = "Standard car",
                    CategoryId = 3,
                    Description = "Standard car. Good enough for most families.",
                    Price = 50000
                }, 
                new Product
                {
                    Id = 8,
                    Name = "Smart car",
                    CategoryId = 3,
                    Description = "Really small and smart car with electric engine. Great in cities!",
                    Price = 35000
                }, 
                new Product
                {
                    Id = 9,
                    Name = "24hour watch",
                    CategoryId = 4,
                    Description = "Unbelievable watch with 24 hours clock face. It can measure time!",
                    Price = 300
                }, 
                new Product
                {
                    Id = 10,
                    Name = "Sports watch",
                    CategoryId = 4,
                    Description = "Good watch for doing sports. Stopwatch, timer, water resists and many other features! Worth to buy, but it's not real, so you cannot... :(",
                    Price = 200
                }, 
                new Product
                {
                    Id = 11,
                    Name = "Expensive watch",
                    CategoryId = 4,
                    Description = "Expensive watch. Exclusive for businessmens and other rich people.",
                    Price = 5000
                }, 
                new Product
                {
                    Id = 12,
                    Name = "Standard coffie machine",
                    CategoryId = 5,
                    Description = "Feauters: makes coffie",
                    Price = 3000
                }
            );
        }
    }
}
