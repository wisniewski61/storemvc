﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Data.Migrations
{
    public partial class SeedProductsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Product",
                columns: new[] { "Id", "CategoryId", "Description", "Image", "Name", "Price", "Rating" },
                values: new object[,]
                {
                    { 1, (byte)1, "This is fictional computer. There should be some sample specification.", null, "Fictional Computer Model 999", 3000m, 0 },
                    { 2, (byte)1, "This is fictional PC computer. It is working great in my mind.", null, "Unreal PC 1333", 1000m, 0 },
                    { 3, (byte)1, "Finctional ultrabook. Just imagine how good it might look.", null, "Ultrabook", 4000m, 0 },
                    { 4, (byte)2, "This mega phone could be used anywhere you want!", null, "Mega Phone 3819", 500m, 0 },
                    { 5, (byte)2, "Super smartphone. Triple boot: Android, iOS and Windows Mobile in one device!!! It is not real at all...", null, "Super smartphone", 2999.99m, 0 },
                    { 6, (byte)3, "This car could win all really series in the world!", null, "Glorious car", 300000.49m, 0 },
                    { 7, (byte)3, "Standard car. Good enough for most families.", null, "Standard car", 50000m, 0 },
                    { 8, (byte)3, "Really small and smart car with electric engine. Great in cities!", null, "Smart car", 35000m, 0 },
                    { 9, (byte)4, "Unbelievable watch with 24 hours clock face. It can measure time!", null, "24hour watch", 300m, 0 },
                    { 10, (byte)4, "Good watch for doing sports. Stopwatch, timer, water resists and many other features! Worth to buy, but it's not real, so you cannot... :(", null, "Sports watch", 200m, 0 },
                    { 11, (byte)4, "Expensive watch. Exclusive for businessmens and other rich people.", null, "Expensive watch", 5000m, 0 },
                    { 12, (byte)5, "Feauters: makes coffie", null, "Standard coffie machine", 3000m, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Product",
                keyColumn: "Id",
                keyValue: 12);
        }
    }
}
