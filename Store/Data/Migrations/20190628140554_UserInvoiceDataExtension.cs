﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Data.Migrations
{
    public partial class UserInvoiceDataExtension : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomTag",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "InvoiceUserDataId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_InvoiceUserDataId",
                table: "AspNetUsers",
                column: "InvoiceUserDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_InvoiceUserData_InvoiceUserDataId",
                table: "AspNetUsers",
                column: "InvoiceUserDataId",
                principalTable: "InvoiceUserData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_InvoiceUserData_InvoiceUserDataId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_InvoiceUserDataId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "InvoiceUserDataId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "CustomTag",
                table: "AspNetUsers",
                nullable: true);
        }
    }
}
