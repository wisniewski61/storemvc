﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Helpers
{
    public class UsersManagerHelper
    {
        protected IServiceProvider _serviceProvider;
        protected RoleManager<IdentityRole> _roleManager;
        protected UserManager<ApplicationUser> _userManager;

        public UsersManagerHelper(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _roleManager = _serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            _userManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
        }

        /// <summary>
        /// Adds two users: admin@store and user@store with password: pass@WORD1 (the same for both users). 
        /// admin@store is added to admin role.
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task SeedUsers()
        {
            const string adminEmail = "admin@store.com";

            await CreateApplicationUser(adminEmail, "pass@WORD1");
            await CreateApplicationUser("user@store.com", "pass@WORD1");

            await CreateUserRoleIfNotExistsAsync(UserRoles.Admin);
            await AssignUserRole(adminEmail, UserRoles.Admin);
        }

        public async Task<IdentityResult> AssignUserRole(string userEmail, string role)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            var roleExists = await _roleManager.RoleExistsAsync(role);

            if (user != null && roleExists)
                return await _userManager.AddToRoleAsync(user, role);

            return IdentityResult.Failed();
        }

        public async Task<IdentityResult> CreateUserRoleIfNotExistsAsync(string role)
        {
            var roleExists = await _roleManager.RoleExistsAsync(role);
            if (!roleExists)
            {
                return await _roleManager.CreateAsync(new IdentityRole(role));
            }

            return IdentityResult.Failed();
        }

        public async Task<IdentityResult> CreateApplicationUser(string email, string password)
        {
            var user = new ApplicationUser
            {
                UserName = email,
                Email = email,
                EmailConfirmed = true
            };

            return await _userManager.CreateAsync(user, password);
        }
    }
}
