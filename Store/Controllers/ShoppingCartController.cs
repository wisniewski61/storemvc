﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.Data;
using Store.Helpers;
using Store.Models;

namespace Store.Controllers
{
    public class ShoppingCartController : Controller
    {
        private const string sessionObjectKey = "ProductsInCart";
        private readonly ApplicationDbContext _context;

        public ShoppingCartController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Add(int productid)
        {
            var viewModel = await GetProductViewModelAsync(productid);
            if (viewModel == null)
                return NotFound();

            return View(viewModel);
        }

        public IActionResult AddConfirmed(ShoppingCartProductViewModel addedProduct)
        {
            if (addedProduct.Product == null || addedProduct.Product.Id < 1)
            {
                return BadRequest(); // todo: maybe redirect is better?
            }// if

            var productInCart = new ProductInCartSessionModel
            {
                ProductId = addedProduct.Product.Id,
                Quantity = addedProduct.Quantity
            };

            return View(productInCart);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> List(ShoppingCartEncodedData shoppingCartData)
        {
            return View(await shoppingCartData.GetShoppingCartItemsAsync(_context));
        }

        public async Task<IActionResult> RemoveItem(int productId, int quantity)
        {
            var viewModel = await GetProductViewModelAsync(productId);
            if (viewModel == null)
                return NotFound();

            viewModel.Quantity = quantity; // todo: is that ok?

            return View(viewModel);
        }

        protected async Task<ShoppingCartProductViewModel> GetProductViewModelAsync(int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(p => p.Id == productId);

            if (product == null)
                return null;

            var viewModel = new ShoppingCartProductViewModel
            {
                Product = product
            };

            return viewModel;
        }
        /*
         * Old code - not used anymore
         * Store shopping cart with session in ASP.NET
        public IActionResult AddConfirmed(ShoppingCartProductViewModel addProduct)
        {
            if (addProduct.Product == null || addProduct.Product.Id < 1)
            {
                return BadRequest();
            }// if

            var productInCart = new ProductInCartSessionModel
            {
                ProductId = addProduct.Product.Id,
                Quantity = addProduct.Quantity
            };

            var productsInCartList = HttpContext.Session.GetObjectFromJson<ProductsInCartListSessionModel>(sessionObjectKey);
            if(productsInCartList == null)
                productsInCartList = new ProductsInCartListSessionModel();

            productsInCartList.Products.Add(productInCart);

            HttpContext.Session.SetObjectAsJson(sessionObjectKey, productsInCartList);
            
            return Content("Added: " + addProduct.Product.Id + " Quantity: " + addProduct.Quantity);
        }//AddConfirmed()

        public IActionResult TestSession()
        {
            var productsInCartList = HttpContext.Session.GetObjectFromJson<ProductsInCartListSessionModel>(sessionObjectKey);

            if (productsInCartList == null)
                return NotFound();

            string contentString = string.Empty;

            foreach(var productInCart in productsInCartList.Products)
            {
                contentString += "ProductId: " + productInCart.ProductId + " Quantity: " + productInCart.Quantity + "\n";
            }

            return Content(contentString);
        }//TestSession()
        */
    }
}