﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.Data;
using Store.Models;

namespace Store.Controllers
{
    public class OrderController : Controller
    {
        // todo: bindings by hand
        protected readonly ApplicationDbContext _context;
        protected readonly UserManager<ApplicationUser> _userManager;

        public OrderController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(ShoppingCartEncodedData shoppingCartEncoded)
        {
            var orderDetailsView = new OrderDetailsView
            {
                ShoppingCartEncoded = shoppingCartEncoded,
                ShoppingCartProducts = await shoppingCartEncoded.GetShoppingCartItemsAsync(_context),
                PaymentProviders = await _context.PaymentProviders.ToListAsync()
            };

            return View(orderDetailsView);
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Summary(OrderDetailsView orderDetails)
        {
            // todo: Terms of use
            orderDetails.PaymentProviders = await _context.PaymentProviders.ToListAsync();

            if(!ModelState.IsValid)
            {
                if (orderDetails.ShoppingCartEncoded != null && 
                    orderDetails.ShoppingCartEncoded.productsString != null &&
                    orderDetails.ShoppingCartEncoded.productsString.Length > 0)
                {
                    orderDetails.ShoppingCartProducts = await orderDetails.ShoppingCartEncoded.GetShoppingCartItemsAsync(_context);
                }
                return View("Index", orderDetails);
            }

            orderDetails.ShoppingCartProducts = await orderDetails.ShoppingCartEncoded.GetShoppingCartItemsAsync(_context);
            return View(orderDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmAndPay(OrderDetailsView orderDetails)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            var order = new Order
            {
                OrderDateTime = DateTime.Now,
                InvoiceUserData = orderDetails.GetInvoiceUserData(),
                DeliveryAddress = orderDetails.GetDeliveryAddress(),
                OrderLines = new List<OrderLine>()
            };

            if(User.Identity.IsAuthenticated)
            {
                order.User = await _userManager.GetUserAsync(User);
            }

            var itemsEncoded = await orderDetails.ShoppingCartEncoded.GetShoppingCartItemsAsync(_context);
            foreach (var item in itemsEncoded)
            {
                var line = new OrderLine
                {
                    Product = item.Product,
                    Quantity = item.Quantity
                };

                order.OrderLines.Add(line);
            };

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            return View();
        }
    }
}