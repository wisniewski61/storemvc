﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.Data;
using Store.Helpers;
using Store.Models;

namespace Store.Controllers
{
    [Authorize(Roles = UserRoles.Admin)]
    public class ManageUsersController : Controller
    {
        // todo: make the methods async -> use _userManager
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ManageUsersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            // todo: find a way to get this data in single query
            var usersList = await _context.Users.ToListAsync();
            var adminRoleId = await GetAdminRoleIdAsync();
            var usersViewList = new List<UserViewModel>();

            foreach (var user in usersList)
            {
                var userView = await CreateUserViewModelAsync(user, adminRoleId);
                usersViewList.Add(userView);
            }//for

            return View(usersViewList);
        }// Index()

        public async Task<IActionResult> Edit(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return NotFound();
            }// if

            var adminRoleId = await GetAdminRoleIdAsync();
            var user = await _context.Users.SingleOrDefaultAsync(u => u.UserName == username);
            var userView = await CreateUserViewModelAsync(user, adminRoleId);

            return View(userView);
        }// Edit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel userView)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(userView.Username);

                if (user == null)
                    return NotFound();

                var isInAdminRole = await _userManager.IsInRoleAsync(user, UserRoles.Admin);

                if (!userView.IsAdmin && isInAdminRole)
                    await _userManager.RemoveFromRoleAsync(user, UserRoles.Admin);
                else if(userView.IsAdmin && !isInAdminRole)
                    await _userManager.AddToRoleAsync(user, UserRoles.Admin);

                return RedirectToAction(nameof(Index));
            }//if

            return View(userView);
        }

        protected async Task<string> GetAdminRoleIdAsync()
        {
            var adminRole = await _context.Roles.SingleOrDefaultAsync(r => r.Name == UserRoles.Admin);
            return adminRole.Id;
        }

        protected async Task<UserViewModel> CreateUserViewModelAsync(IdentityUser user, string adminRoleId)
        {
            var userRole = await _context.UserRoles.SingleOrDefaultAsync(r => r.UserId == user.Id && r.RoleId == adminRoleId);

            var userView = new UserViewModel
            {
                Username = user.UserName,
                IsAdmin = userRole != null
            };

            return userView;
        }
    }
}