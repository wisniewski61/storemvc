﻿var submitShoppingCartHiddenForm = function (shoppingCart, method, action) {
    let products = shoppingCart.getProductsList();

    if (products != undefined && products != null) {
        let productListString = "";

        for (let i = 0; i < products.length; i++) {
            productListString += products[i].productId + ":" + products[i].quantity + ";";
        }

        $("#productsString").val(productListString);
    }//if

    let form = $("#products-string-form");
    form.attr("action", action);
    form.attr("method", method);
    form.submit();
}