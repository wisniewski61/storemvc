﻿// todo: check if there is better (and modern) approach.
var shoppingCart = {
    localStorageKey: "addedProductsList",
    
    getProductsList: function () {
        return JSON.parse(localStorage.getItem(this.localStorageKey));
    },

    updateProductsList: function (localStorageProductsList) {
        localStorage.setItem(this.localStorageKey, JSON.stringify(localStorageProductsList));
    },

    addProduct: function (productData) {
        let localStorageProductsList;
        if (localStorage.length > 0) {
            localStorageProductsList = this.getProductsList();
        }//if

        if (localStorageProductsList == undefined || localStorageProductsList == null) {
            localStorageProductsList = [];
        }

        let isOnList = false;
        for (let i = 0; i < localStorageProductsList.length; i++) {
            if (localStorageProductsList[i].productId == productData.productId) {
                localStorageProductsList[i].quantity += productData.quantity;
                isOnList = true;
                break;
            }//if
        }//for

        if (!isOnList)
            localStorageProductsList.push(productData);
        this.updateProductsList(localStorageProductsList);
    },

    removeProduct: function (productId) {
        console.log('productID: ' + productId);
        let products = this.getProductsList();

        if (products != undefined && products != null) {
            for (let i = 0; i < products.length; i++) {
                if (products[i].productId === productId) {
                    products.splice(i, 1);
                    this.updateProductsList(products);
                    break;
                }//if
            }//for
        }//if
    },

    clearProductsList: function () {
        localStorage.removeItem(this.localStorageKey);
    }
}