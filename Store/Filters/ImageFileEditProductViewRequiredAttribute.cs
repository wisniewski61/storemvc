﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Filters
{
    /// <summary>
    /// Validation class for EditProductViewModel's ImageFile field
    /// </summary>
    public class ImageFileEditProductViewRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var editProductViewModel = (EditProductViewModel)validationContext.ObjectInstance;

            if((editProductViewModel.Image != null && editProductViewModel.Image.Length > 0) ||
                (editProductViewModel.ImageFile != null && editProductViewModel.ImageFile.Length > 0))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("The Image field is required."); // todo: replace string with property or something
        }
    }
}
